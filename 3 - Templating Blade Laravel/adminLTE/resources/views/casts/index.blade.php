@extends('master')

@section('content_title')
  Casts
@endsection

@section('card_title')
  Casts List
@endsection

@section('content')
@if (session('success'))
<div class="alert alert-success">
    {{ session('success') }}
</div>
@endif
<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col" class="text-center">ID</th>
        <th scope="col" class="text-center">Nama</th>
        <th scope="col" class="text-center">Umur</th>
        <th scope="col" class="text-center">Bio</th>
        <th scope="col" class="text-center">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($casts as $key=>$cast)
            <tr>
                <th scope="row">{{ $cast->id }}</th>
                <td>{{ $cast->nama }}</td>
                <td>{{ $cast->umur }}</td>
                <td>{{ $cast->bio }}</td>
                <td class="d-flex justify-content-around">
                    <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">Show</a>
                    <a href="/cast/{{$cast->id}}/edit" class="btn btn-secondary btn-sm">Edit</a>
                    <form action="/cast/{{$cast->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4" class="text-center">No Casts</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection