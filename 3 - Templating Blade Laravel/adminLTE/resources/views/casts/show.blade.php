@extends('master')

@section('content_title')
  Cast
@endsection

@section('card_title')
  {{ $cast->nama }}
@endsection

@section('content')
    <p class="font-weight-light">{{ $cast->umur }} years old</p>
    <p>{{ $cast->bio }}</p>
@endsection