<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook</title>
</head>
<body>
    <div class="opening">
        <h1>SanberBook</h1>
        <h2>Social Media Developer Santai Berkualitas</h2>
        <p>Belajar dan Berbagi agar hidup ini semakin berkualitas</p>
    </div>
    <div class="benefit">
        <h2>Benefit Join di Sanberbook</h2>
        <ul>
            <li>Mendapatkan motivasi dari sesama developer</li>
            <li>Sharing knowledge dari para mastah sanber</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>
    </div>
    <div class="bergabung">
        <h2>Cara Bergabung ke SanberBook</h2>
        <ol>
            <li>Mengunjungi website ini</li>
            <li>Mendaftar di <a href='/register'>Form Sign Up</a></li>
            <li>Selesai!</li>
        </ol>
    </div>
</body>
</html>