<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sanberbook</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name: </label> <br><br>
        <input type="text" name="firstName"> <br><br>
        <label>Last Name: </label> <br><br>
        <input type="text" name="lastName"> <br><br>
        <label>Gender: </label> <br><br>
        <input type="radio" id="male" name="gender" value="male">Male <br>
        <input type="radio" id="female" name="gender" value="female">Female <br>
        <input type="radio" id="other" name="gender" value="other">Other <br> <br>
        <label>Nationality: </label> <br><br>
        <select name="nationality"> <br>
            <option value="indonesia">Indonesia</option>
            <option value="thailand">Thailand</option>
            <option value="vietnam">Vietnam</option>
        </select> <br><br>
        <label">Language Spoken: </label> <br><br>
        <input type="checkbox" name="languange[]" id="indonesia" value="indo">Bahasa Indonesia <br> 
        <input type="checkbox" name="languange[]" id="english" value="english">English <br>
        <input type="checkbox" name="languange[]" id="other" value="other">Other <br><br>
        <label>Bio: </label> <br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea> <br>
        <button type="submit">Sign Up</button>
    </form>
</body>
</html>